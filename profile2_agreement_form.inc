<?php

/**
 * @file
 * Builds profile-specific agreement forms.
 */

/**
 * Page callback: Displays a user agreement form.
 *
 * Path: [path-setting]/agreement
 *
 * @param object $profiles
 *   Object containing single row from profile2_agreement_get_profiles() database
 *   result.
 *
 * @see profile2_agreement_menu()
 */
function profile2_agreement_page($profiles) {
  $page_title = _profile2_agreement_get_data($profiles, 'page_title');
  //@todo get and set the name of organization
  $title = !empty($page_title) ? $page_title : t('  %name', array('%name' => ''));
  drupal_set_title($title, PASS_THROUGH);

  $form_state = array(
    'profiles' => $profiles,
  );

  return drupal_build_form('profile2_agreement_form', $form_state);
}

/**
 * Builds profile-specific agreement forms.
 */
function profile2_agreement_form($form, &$form_state) {
  $profiles = $form_state['profiles'];

  $form = array();
  $form['title'] = array(
    '#type' => 'item',
    '#title' => _profile2_agreement_get_data($profiles, 'agreement_title'),
    '#weight' => 0,
  );
  $form['agreement'] = array(
    '#type' => 'fieldset',
    '#title' => t('Terms and Conditions of Use'),
  );
  $form['agreement']['body'] = array(
    '#type' => 'textarea',
    '#default_value' => _profile2_agreement_get_data($profiles, 'body'),
    '#rows' => 10,
    '#attributes' => array('readonly' => 'readonly'),
  );
  $form['agree'] = array(
    '#type' => 'submit',
    '#value' => t('I Agree'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}

function profile2_agreement_form_validate($form, &$form_state) {
  // Gets values
  $profile = $form_state['profiles'];
  $clicked_button = $form_state['clicked_button']['#id'];

  switch ($clicked_button) {
    case 'edit-agree':
      $form_state['redirect'] = $profile[0]->success_redirect . '/register';
      break;
    case 'edit-cancel':
      $form_state['redirect'] = $profile[0]->cancel_redirect;
      break;
    default:
      break;
  }
}

/**
 * Sets page title for agreement pages.
 *
 * @param object $profiles
 *   Object containing single row from profile2_agreement_get_profiles() database
 *   result.
 *
 * @param string $key
 *   Array key for 'misc' array. This will determine the title settings.
 */
function _profile2_agreement_get_data($profiles, $key) {
  // Look for custom title in foremost profile, according to weight.
  if (isset($profiles[0]->misc) && $misc = unserialize($profiles[0]->misc)) {
    if (array_key_exists($key, $misc)) {
      return $misc[$key];
    }
  }
}
